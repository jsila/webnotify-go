package main

import (
	"net/http"
	"sort"

	"github.com/matryer/respond"
)

func InitNotificationsController(c *Controller) {
	r := c.Router
	r.Use(c.checkBasicAuth)

	nc := &NotificationsController{c}
	r.Get("/", nc.Index)
	r.With(nc.CheckNotificationJSON).Post("/", nc.Save)
}

type NotificationsController struct {
	*Controller
}

func (c *NotificationsController) Index(w http.ResponseWriter, r *http.Request) {
	ns, err := c.DB.GetNotifications()
	if err != nil {
		c.Log.Errorf("can't get notifications: %v", err)
		respond.WithStatus(w, r, http.StatusInternalServerError)
		return
	}
	sort.Sort(ns)
	c.Render.HTML(w, http.StatusOK, "index", ns)
}

func (c *NotificationsController) Save(w http.ResponseWriter, r *http.Request) {
	n := r.Context().Value("notification").(*Notification)

	code := http.StatusCreated
	if err := c.DB.SaveNotification(n); err != nil {
		code = http.StatusInternalServerError
	}

	respond.WithStatus(w, r, code)
}

func (c *NotificationsController) CheckNotificationJSON(h http.Handler) http.Handler {
	return c.CheckJSON(h, &Notification{}, "notification")
}
