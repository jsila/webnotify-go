# Web Notify Website

App that receives notification from Android app and displays them as a feed from latest to oldest.

It's protected so user needs username and password to be able to view his or her notifications.

See screenshots folder for app preview (both Android and web).

Also on my account you can find Android app which accompany this web app.