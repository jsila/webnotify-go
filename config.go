package main

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

type AuthUser struct {
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

func getConfig(filename string, o interface{}) error {
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(file, o); err != nil {
		return err
	}
	return nil
}
