package main

import (
	"log"
	"net/http"
)

func main() {
	env := NewEnvironment()
	InitControllers(env)

	http.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))
	http.Handle("/", env.Router)

	log.Println("Listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
