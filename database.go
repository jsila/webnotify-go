package main

import (
	"fmt"

	"time"

	"strconv"

	"github.com/go-redis/redis"
)

type Dataset interface {
	GetNotifications() (Notifications, error)
	SaveNotification(*Notification) error
}

type DB struct {
	*redis.Client
}

func (db *DB) SaveNotification(n *Notification) error {
	err := db.Watch(func(tx *redis.Tx) error {
		c, err := tx.Incr("notifications:count").Result()
		if err != nil {
			return err
		}
		n.ID = c
		key := fmt.Sprintf("notifications:%d", c)

		if err := tx.HMSet(key, n.Map()).Err(); err != nil {
			return err
		}
		d := time.Duration(n.Retention) * time.Hour
		if err := tx.Expire(key, d).Err(); err != nil {
			return err
		}
		return nil
	})
	return err
}

func (db *DB) GetNotifications() (Notifications, error) {
	notifications := Notifications{}
	err := db.Watch(func(tx *redis.Tx) error {
		res, err := tx.Get("notifications:count").Result()
		if err != nil {
			return err
		}

		n, err := strconv.Atoi(res)
		if err != nil {
			return err
		}

		for i := 1; i <= n; i++ {
			nMap, err := tx.HGetAll(fmt.Sprintf("notifications:%d", i)).Result()
			if err != nil {
				return err
			}
			if len(nMap) == 0 {
				continue
			}
			n, err := NotificationFromMap(nMap)
			if err != nil {
				return err
			}
			notifications = append(notifications, n)
		}
		return nil
	})
	return notifications, err
}
