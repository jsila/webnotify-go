package main

import "github.com/rs/cors"
import "net/http"

type Middleware func(http.Handler) http.Handler

func Cors() Middleware {
	return cors.New(cors.Options{
		AllowCredentials: true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
	}).Handler
}
