package main

import (
	"html/template"

	"github.com/Sirupsen/logrus"
	"github.com/go-redis/redis"
	"github.com/pressly/chi"
	"github.com/unrolled/render"
)

type Config struct {
	Redis struct {
		Addr string
	} `yaml:"redis"`
	Users []*AuthUser `yaml:"users"`
}

type Environment struct {
	Router *chi.Mux
	DB     Dataset
	Log    *logrus.Logger
	Render *render.Render
	Config *Config
}

func NewEnvironment() *Environment {
	log := logrus.New()

	cnfg := &Config{}

	if err := getConfig("./config.yml", cnfg); err != nil {
		log.Panicf("can't get config: %v", err)
	}

	db := redis.NewClient(&redis.Options{
		Addr:     cnfg.Redis.Addr,
		Password: "",
		DB:       0,
	})

	router := chi.NewRouter()

	if err := db.Ping().Err(); err != nil {
		log.Panicf("can't ping redis database: %v", err)
	}

	templateHelpers := map[string]interface{}{
		"relative_time": RelativeTimestamps,
	}

	rndr := render.New(render.Options{
		Funcs: []template.FuncMap{templateHelpers},
	})

	return &Environment{
		Router: router,
		DB:     &DB{db},
		Log:    log,
		Render: rndr,
		Config: cnfg,
	}
}
