package main

import "time"
import "strconv"

type Notification struct {
	ID        int64     `json:"id"`
	Title     string    `json:"title"`
	Text      string    `json:"text"`
	Package   string    `json:"package"`
	Retention int       `json:"retention"`
	Timestamp time.Time `json:"timestamp"`
	App       string    `json:"app"`
}

func NotificationFromMap(n map[string]string) (*Notification, error) {
	r, err := strconv.Atoi(n["retention"])
	if err != nil {
		return nil, nil
	}
	t, err := time.Parse(time.RFC3339, n["timestamp"])
	if err != nil {
		return nil, err
	}
	id, err := strconv.ParseInt(n["id"], 10, 64)
	if err != nil {
		return nil, err
	}
	notification := &Notification{
		ID:        id,
		Title:     n["title"],
		Text:      n["text"],
		Package:   n["package"],
		App:       n["app"],
		Retention: r,
		Timestamp: t,
	}
	return notification, nil
}

func (n *Notification) Map() map[string]interface{} {
	return map[string]interface{}{
		"id":        n.ID,
		"title":     n.Title,
		"text":      n.Text,
		"package":   n.Package,
		"app":       n.App,
		"retention": n.Retention,
		"timestamp": n.Timestamp.Format(time.RFC3339),
	}
}

type Notifications []*Notification

func (n Notifications) Len() int {
	return len(n)
}

func (n Notifications) Less(i, j int) bool {
	return n[i].Timestamp.After(n[j].Timestamp)
}

func (n Notifications) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}
