package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/matryer/respond"
	"github.com/xeipuuv/gojsonschema"
)

func InitControllers(env *Environment) {
	c := &Controller{Environment: env}

	r := env.Router
	r.Use(Cors())

	InitNotificationsController(c)
}

type Controller struct {
	*Environment
}

func (c *Controller) CheckJSON(h http.Handler, data interface{}, contextKey string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		valid, err := c.ReadAndValidate(r, NotificationJSONSchema, data)

		if err != nil {
			c.Log.Errorf("can't read and validate data against schema: %v", err)
			respond.WithStatus(w, r, http.StatusInternalServerError)
			return
		}

		if !valid {
			respond.WithStatus(w, r, http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), contextKey, data)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (c *Controller) ReadAndValidate(r *http.Request, schema string, n interface{}) (bool, error) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return false, err
	}
	defer r.Body.Close()
	documentLoader := gojsonschema.NewBytesLoader(b)
	schemaLoader := gojsonschema.NewStringLoader(schema)

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)

	if err != nil {
		return false, fmt.Errorf("can't create validator: %v", err)
	}

	if !result.Valid() {
		c.Log.Println(result.Errors())
		return false, nil
	}

	if err := json.NewDecoder(bytes.NewReader(b)).Decode(&n); err != nil {
		return false, fmt.Errorf("can't decode json: %v", err)
	}

	return true, nil
}

func (c *Controller) checkBasicAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Header().Set("WWW-Authenticate", `Basic realm="Notifications"`)
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}
		for _, user := range c.Config.Users {
			if user.Username == username && user.Password == password {
				h.ServeHTTP(w, r)
				return
			}
		}
		w.Header().Set("WWW-Authenticate", `Basic realm="Notifications"`)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	})
}
