package main

import (
	"fmt"
	"time"
)

func pluralize(s string, n float64) string {
	if int(n) > 1 {
		s += "s"
	}
	return s
}
func RelativeTimestamps(t time.Time) string {
	since := time.Since(t)
	var n int
	var p string
	if h := since.Hours(); int(h) > 0 {
		n = int(h)
		p = pluralize("hour", h)
	} else if m := since.Minutes(); int(m) > 0 {
		n = int(m)
		p = pluralize("minute", m)
	} else if s := since.Seconds(); int(s) > 0 {
		n = int(s)
		p = pluralize("second", s)
	}
	return fmt.Sprintf("%d %s ago", n, p)
}
