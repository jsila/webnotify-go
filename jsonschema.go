package main

var NotificationJSONSchema = `{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions": {},
    "id": "http://example.com/example.json",
    "properties": {
        "package": {
            "id": "/properties/package",
            "type": "string"
        },
        "retention": {
            "id": "/properties/retention",
            "type": "integer"
        },
        "text": {
            "id": "/properties/text",
            "type": "string"
        },
        "timestamp": {
            "id": "/properties/timestamp",
            "type": "string"
        },
        "title": {
            "id": "/properties/title",
            "type": "string"
        }
    },
	"required": [
		"title", "text", "timestamp", "retention", "package"
	],
    "type": "object"
}`
